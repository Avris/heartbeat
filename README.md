# heartbeat

**lightweight website monitoring**

it:
 - checks configured URLs for an HTTP 2xx response (or applies custom validation),
 - generates static HTML status page,
 - and sends out notifications when necessary.

simple as that.

## usage

```
mkdir heartbeat.example.com
cd heartbeat.example.com
pnpm add avris-heartbeat
node_modules/.bin/heartbeat check --init
```

with any command you can specify a different location than the cwd using `-d /path/to/project`

modify the generated `heartbeat.mjs` to your needs.
you can keep the secrets in `.env` file, it will be loaded into `process.env` automatically.

set up cronjobs:

```
* * * * *        cd /path/to/project; node_modules/.bin/heartbeat check 
*/15 * * * *     cd /path/to/project; node_modules/.bin/heartbeat generate
0 1 * * *        cd /path/to/project; node_modules/.bin/heartbeat cleanup
```

point your web server to serve static files from the `./public` directory.
