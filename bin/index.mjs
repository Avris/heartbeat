#!/usr/bin/env node

import { Command } from 'commander';
import wrapper from '../src/commands/wrapper.mjs';
import check from '../src/commands/check.mjs';
import generate from '../src/commands/generate.mjs';
import dailies from '../src/commands/dailies.mjs';
import vacuum from '../src/commands/vacuum.mjs';

const program = new Command();

program
    .name('heartbeat')
    .description('lightweight website monitoring')
    .version('3.0.0')
    .option('-d, --dir <path>', 'Working directory')
    .option('-c, --config <path>', 'Path to the configuration file', './heartbeat.mjs')
    .option('--data <path>', 'Path to the data directory (database, caches)', './data')
    .option('--public <path>', 'Path to the public directory (generated website)', './public')
    .option('--init', 'Initialise a project with template configuration')
    .option('--fixtures', 'Generate mock data when running the `check` command')
;

program
    .command('check')
    .description('Performs all checks and saves the results to the database')
    .action(wrapper(program.opts(), async (config, db, options) => {
        const hasChanges = await check(config, db, options);
        if (hasChanges) {
            await generate(config, db);
        }
    }))
;

program
    .command('generate')
    .description('Generates static website content')
    .action(wrapper(program.opts(), async (config, db, options) => {
        await generate(config, db);
    }))
;

program
    .command('dailies')
    .description('Aggregates minute-level data into daily summaries')
    .action(wrapper(program.opts(), async (config, db, options) => {
        await dailies(config, db);
    }))
;

program
    .command('vacuum')
    .description('Cleans up old data that')
    .action(wrapper(program.opts(), async (config, db, options) => {
        await dailies(config, db);
        await vacuum(config, db);
    }))
;

program.parse(process.argv);
