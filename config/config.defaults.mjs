import StdoutNotifier from '../src/notifiers/StdoutNotifier.mjs';

export default async () => ({
    baseUrl: '/',
    hideExtensions: true,
    timeout: 3000,
    slotsCount: 96,
    periods: {
        'live': 96,
        '24h': 24 * 60,
        '7d': 7 * 24 * 60,
        '30d': 30 * 24 * 60,
    },
    defaultPeriod: '24h',
    notifyAfter: 10,
    notifiers: [
        new StdoutNotifier(),
    ],
    cleanupAfterDays: 90,
});
