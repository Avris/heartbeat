import StdoutNotifier from "avris-heartbeat/src/notifiers/StdoutNotifier.mjs";
import EmailNotifier from "avris-heartbeat/src/notifiers/EmailNotifier.mjs";
import HttpCheck from "avris-heartbeat/src/checks/HttpCheck.mjs";
import RetryCheck from "avris-heartbeat/src/checks/RetryCheck.mjs";

export default async (paths) => ({
    project: 'my project',
    pages: {
        'example.com': new RetryCheck(new HttpCheck({url: `https://example.com`})),
    },
    notifiers: [
        new StdoutNotifier(),
        new EmailNotifier(
            process.env.MAILER_URL,
            'from@example.com',
            [
                'to@example.com',
            ]
        ),
    ],
});
