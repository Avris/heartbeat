import dns from 'dns';
import ip6addr from 'ip6addr';

export default class DnsCheck {
    constructor({servers, domain, type, expectedResults}) {
        this.resolver = new dns.promises.Resolver();
        if (servers) {
            this.resolver.setServers(Array.isArray(servers) ? servers : [servers]);
        }
        this.domain = domain;
        this.type = type;
        this.expectedResults = Array.isArray(expectedResults) ? expectedResults : [expectedResults];
    }

    name() {
        return this.domain;
    }

    async check() {
        const results = await this.resolver.resolve(this.domain, this.type);

        for (let result of results) {
            for (let expectedResult of this.expectedResults) {
                if (this._resultMatchesExpected(result, expectedResult)) {
                    return true;
                }
            }
        }

        return false;
    }

    _resultMatchesExpected(result, expected) {
        if (result === expected) {
            return true;
        }

        try {
            const address = ip6addr.parse(result);
            const subnet = ip6addr.createCIDR(expected);
            return subnet.contains(address);
        } catch {
            return false;
        }
    }
}
