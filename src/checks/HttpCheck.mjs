import fetch from 'node-fetch';
import {DEFAULT_TIMEOUT} from "../const.mjs";

export default class HttpCheck {
    constructor({
        url,
        params = {},
        validation = async (r) => r.status >= 200 && r.status < 300,
        timeout = DEFAULT_TIMEOUT,
    }) {
        this.url = url;
        this.params = params;
        this.validation = validation;
        this.timeout = timeout;
    }

    name() {
        return this.url;
    }

    async check() {
        const AbortController = globalThis.AbortController || (await import('abort-controller')).default;

        const controller = new AbortController()
        const controllerTimeout = setTimeout(() => {
            controller.abort();
        }, this.timeout);

        try {
            const r = await fetch(this.url, {
                signal: controller.signal,
                ...this.params,
            });
            return await this.validation(r);
        } catch {
            return false;
        } finally {
            clearTimeout(controllerTimeout);
        }
    }
}
