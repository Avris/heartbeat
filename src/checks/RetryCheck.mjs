import {buildProxy, sleep} from "../helpers.mjs";

export default class RetryCheck {
    constructor(base, times = 3, sleep = 100) {
        this.base = base;
        this.times = times;
        this.sleep = sleep;

        return buildProxy(this);
    }

    name() {
        return `retry(${this.base.name()})`;
    }

    async check() {
        let retries = 0;
        while (retries++ < this.times) {
            let result = false;
            try {
                result = await this.base.check();
            } catch {}
            if (result) {
                return true;
            }
            await sleep(this.sleep);
        }

        return false;
    }
}
