import SQL from "sql-template-strings";
import chalk from "chalk";
import {GetStatusName, STATUS} from "../const.mjs";
import notify from "./notify.mjs";
import { withTimeout } from "../helpers.mjs";

const colour = (status) => {
    const c = status === STATUS.online
        ? chalk.green
        : (status === STATUS.offline ? chalk.red : chalk.gray);
    return c(GetStatusName(status));
}

const saveResult = async (db, page, result, started, finished) => {
    const responseTime = finished - started;
    const status = result ? STATUS.online : STATUS.offline;

    console.log(`${page}\n  ${colour(status)} (${responseTime} ms)`);

    await db.get(SQL`INSERT INTO checks (page, timestamp, status, responseTime) VALUES (
        ${page},
        ${Math.floor(started / 1000)},
        ${status},
        ${responseTime}
    );`);

    return result;
};

async function insertFixtures(db, page, start) {
    for (let i = 0; i <= 2880; i++) {
        await saveResult(
            db,
            page,
            Math.random() <= 0.99,
            start - i * 60 * 1000,
            start - i * 60 * 1000 + 100 + Math.random() * 300,
        )
    }
}

export default async (config, db, options) => {
    const {pages, notifyAfter, timeout} = config;

    const overallStart = new Date();
    const checkPromises = [];
    const results = {};
    let hasChanges = false;

    for (let [page, check] of Object.entries(pages)) {
        const start = new Date();

        if (options.fixtures) {
            await insertFixtures(db, page, start);
            results[page] = true;
            hasChanges = true;
            continue;
        }

        checkPromises.push((async () => {
            try {
                const r = await withTimeout(check.check(), timeout);
                results[page] = await saveResult(db, page, r, start, new Date());
            } catch (err) {
                results[page] = await saveResult(db, page, false, start, new Date());
            }
        })());
    }

    await Promise.all(checkPromises);

    let overallStatus = STATUS.unknown;
    const hasOnline = Object.values(results).some(r => r === true);
    const hasOffline = Object.values(results).some(r => r === false);
    if (hasOnline && !hasOffline) { overallStatus = STATUS.online; }
    if (!hasOnline && hasOffline) { overallStatus = STATUS.offline; }

    console.log();
    console.log('overall status:', colour(overallStatus));

    const recents = await db.all(SQL`SELECT timestamp, status FROM recents ORDER BY timestamp DESC LIMIT ${notifyAfter}`);
    if (recents.length) {
        if (recents[0].status !== overallStatus) {
            hasChanges = true;
        }

        if (recents[recents.length - 1].status === STATUS.online
            && recents.slice(0, recents.length - 1).every(s => s.status <= STATUS.unknown)
            && overallStatus <= STATUS.unknown
        ) {
            await notify(overallStatus, results, config); // servers down
        }

        if (recents.every(s => s.status <= STATUS.unknown) && overallStatus === STATUS.online) {
            await notify(overallStatus, results, config); // back up
        }

        if (recents.length >= notifyAfter) {
            await db.get(SQL`DELETE FROM recents WHERE timestamp <= ${recents[recents.length - 1].timestamp}`);
        }
    } else {
        hasChanges = true;
    }

    await db.get(SQL`INSERT INTO recents(timestamp, status) VALUES (${Math.floor(overallStart / 1000)}, ${overallStatus})`);

    return hasChanges;
}
