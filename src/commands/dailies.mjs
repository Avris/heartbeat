import SQL from "sql-template-strings";
import { DateTime } from "luxon";

const fetchRawDailyAggregates = async (db) => {
    const latestDaily = (await db.get(SQL`SELECT max(date) FROM dailies`))['max(date)'];
    console.log(`Latest daily in db: ${latestDaily}`);

    const lastFinishedDay = DateTime.utc().startOf('day');
    console.log(`Last finished day: ${lastFinishedDay.toString()}`)

    const where = latestDaily ?  SQL`
        WHERE timestamp > ${
            DateTime.fromISO(latestDaily, { zone: 'utc' })
                .plus({ days: 1 })
                .startOf('day')
                .toSeconds()
        } AND timestamp < ${lastFinishedDay.toSeconds()}
    ` : SQL`
        WHERE timestamp < ${lastFinishedDay.toSeconds()}
    `;

    return await db.all(SQL`
        SELECT 
            page,
            strftime('%Y-%m-%d', timestamp, 'unixepoch') as date,
            status,
            COUNT(*) as checksCount,
            AVG(checks.responseTime) as responseTimeAvg
        FROM 
            checks`
        .append(where).append(SQL`
        GROUP BY 
            page, date, status
        ORDER BY
            page, date, status;
    `));
}

const calculateDailies = (checksAggregatedRaw) => {
    const dailies = {};
    for (const {page, date, status, checksCount, responseTimeAvg} of checksAggregatedRaw) {
        const key = `${page};${date}`;
        if (status === -1) {
            dailies[key] = {
                page,
                date,
                checksCount,
                uptimePercent: 0.0,
                responseTimeAvg: 0.0,
            }
        } else if (status === 1) {
            if (dailies.hasOwnProperty(key)) {
                const failureDaily = dailies[key];
                dailies[key] = {
                    page,
                    date,
                    checksCount: checksCount + failureDaily.checksCount,
                    uptimePercent: 100.0 * checksCount / (checksCount + failureDaily.checksCount),
                    responseTimeAvg: responseTimeAvg,
                }
            } else {
                dailies[key] = {
                    page,
                    date,
                    checksCount: checksCount,
                    uptimePercent: 100.0,
                    responseTimeAvg: responseTimeAvg,
                }
            }
        }
    }

    console.log(`Calculated ${Object.keys(dailies).length} dailies`);

    return Object.values(dailies);
}

async function insertDailies(db, dailies) {
    try {
        await db.exec('BEGIN TRANSACTION');
        const stmt = await db.prepare(SQL`
            INSERT INTO dailies (page, date, checksCount, uptimePercent, responseTimeAvg)
            VALUES (?, ?, ?, ?, ?)
        `);

        for (const daily of dailies) {
            await stmt.run(daily.page, daily.date, daily.checksCount, daily.uptimePercent, daily.responseTimeAvg);
        }

        await stmt.finalize();
        await db.exec('COMMIT');
    } catch (error) {
        await db.exec('ROLLBACK');
        throw error;
    }
}

export default async (config, db) => {
    const checksAggregatedRaw = await fetchRawDailyAggregates(db);
    const dailies = calculateDailies(checksAggregatedRaw);

    if (dailies.length > 0) {
        await insertDailies(db, dailies);
    }
}
