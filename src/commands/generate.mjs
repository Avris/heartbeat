import calcStats from './stats.mjs';
import { promises as fs } from 'fs';
import { GetStatusName, STATUS } from "../const.mjs";
import path from 'path';

export default async (config, db) => {
    const {defaultPeriod, pages, periods, slotsCount} = config;

    for (let period in periods) {
        const pagesStats = {};
        const pagesStatsShort = {};
        let hasOnline = false;
        let hasOffline = false;

        for (let [page, pageConfig] of Object.entries(pages)) {
            const pageStats = {
                url: pageConfig.url,
                ...await calcStats(page, period, { pages, periods, slotsCount }, db),
            }

            pagesStats[page] = pageStats
            pagesStatsShort[page] = {...pageStats, slots: undefined}

            if (pageStats.status === GetStatusName(STATUS.online)) { hasOnline = true; }
            if (pageStats.status === GetStatusName(STATUS.offline)) { hasOffline = true; }
        }

        let overallStatus = STATUS.unknown;
        if (hasOnline && !hasOffline) { overallStatus = STATUS.online; }
        if (!hasOnline && hasOffline) { overallStatus = STATUS.offline; }

        const json = JSON.stringify({
            status: GetStatusName(overallStatus),
            pages: pagesStatsShort,
        });

        const Template = (await import('../../template/Layout.mjs')).default;

        const html = await Template({
            status: GetStatusName(overallStatus),
            pages: pagesStats,
            period,
            config,
        });

        const saveFile = async (basename, extension, content) => {
            const target = path.resolve(config.paths.public, `${basename}.${extension}`);
            console.log('Writing to', target);
            await fs.writeFile(target, content);
        }

        if (period === defaultPeriod) {
            await saveFile('index', 'html', html);
        }
        await saveFile(period, 'json', json);
        await saveFile(period, 'html', html);
    }
};
