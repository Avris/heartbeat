import { promises as fs } from 'fs';
import { LIB_DIR } from "../const.mjs";
import path from 'path';
import chalk from "chalk";

export default async (configPath) => {
    try {
        await fs.access(configPath);
        console.log(`Config file already exists at ${chalk.green(configPath)}`);
    } catch (error) {
        await fs.copyFile(
            path.resolve(LIB_DIR, 'config/config.template.mjs'),
            configPath,
        );
        console.log(`Config file created at ${chalk.green(configPath)}`);
    }
};
