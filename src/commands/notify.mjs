import {GetStatusName, STATUS} from "../const.mjs";
import T from "../../template/Translations.mjs";


export default async (overallStatus, pagesStatuses, config) => {
    const overallMessage = T.status[GetStatusName(overallStatus)].emoji + ' ' + T.status[GetStatusName(overallStatus)].overall;
    const messagesLookup = {
        true: T.status[GetStatusName(STATUS.online)].emoji + ' ' + T.status[GetStatusName(STATUS.online)].label,
        false: T.status[GetStatusName(STATUS.offline)].emoji + ' ' + T.status[GetStatusName(STATUS.offline)].label,
    }

    // order by status first, then keep the same order as in the config
    const pagesMessages = {};
    for (let page of Object.keys(config.pages)) {
        if (!pagesStatuses[page]) {
            pagesMessages[page] = messagesLookup[false];
        }
    }
    for (let page of Object.keys(config.pages)) {
        if (pagesStatuses[page]) {
            pagesMessages[page] = messagesLookup[true];
        }
    }

    for (let notifier of config.notifiers) {
        await notifier.notify(overallMessage, pagesMessages, config);
    }
}
