import {GetStatusName, STATUS} from '../const.mjs';
import SQL from 'sql-template-strings';

export default async (page, period, { pages, periods, slotsCount }, db) => {
    if (!Object.keys(periods).includes(period)) {
        throw `Invalid period "${period}" (allowed: ${Object.keys(periods).join(', ')})`;
    }
    if (!pages.hasOwnProperty(page)) {
        throw `Unknown page "${page}"`;
    }

    const now = Math.ceil(new Date() / 1000);
    const start = now - periods[period] * 60;
    const slotSize = periods[period] / slotsCount * 60;

    const rows = await db.all(SQL`
        SELECT timestamp, status, responseTime
        FROM checks
        WHERE page = ${page} AND timestamp <= ${now} AND timestamp >= ${start}
        ORDER BY timestamp
    `);

    const slotsRaw = {};
    for (let slotStart = start; slotStart < now; slotStart += slotSize) {
        slotsRaw[slotStart] = { statuses: [], responseTimes: [] }
    }

    let status = 0;
    for (let row of rows) {
        const slotStart = Math.floor((row.timestamp - start) / slotSize) * slotSize + start;
        slotsRaw[slotStart].statuses.push(row.status);
        if (row.status === STATUS.online) {
            slotsRaw[slotStart].responseTimes.push(row.responseTime);
        }

        status = row.status;
    }

    let onlineCount = 0;
    let checksCount = 0;
    let responseTimeSum = 0;
    const slots = {};
    for (let [slotStart, slotStats] of Object.entries(slotsRaw)) {
        checksCount += slotStats.statuses.length;
        onlineCount += slotStats.statuses.filter(s => s === STATUS.online).length;
        const slotResponseTimeSum = slotStats.responseTimes.length
            ? slotStats.responseTimes.reduce((a, b) => a + b)
            : 0;
        responseTimeSum += slotResponseTimeSum;

        slots[slotStart] = {
            status: GetStatusName(slotStats.statuses.length ? Math.min(...slotStats.statuses) : STATUS.unknown),
            avgResponseTime: slotStats.responseTimes.length
                ? Math.round(slotResponseTimeSum / slotStats.responseTimes.length)
                : 0,
            checks: slotStats.statuses.length,
            onlineChecks: slotStats.statuses.filter(s => s === STATUS.online).length,
        }
    }

    return {
        status: GetStatusName(status),
        uptime: checksCount ? Math.round(10000 * onlineCount / checksCount) / 100 : 0,
        avgResponseTime: onlineCount ? Math.round(responseTimeSum / onlineCount) : 0,
        slots,
    };
}
