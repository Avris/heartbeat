import SQL from "sql-template-strings";
import { DateTime } from "luxon";

export default async ({cleanupAfterDays}, db) => {
    const cutoff = DateTime.utc()
        .minus({ days: cleanupAfterDays})
        .startOf('day');

    console.log(`Cleaning up checks before ${cutoff.toString()}`)

    await db.get(SQL`DELETE FROM checks WHERE timestamp <= ${cutoff.toSeconds()}`);
    await db.exec(SQL`VACUUM`);
}
