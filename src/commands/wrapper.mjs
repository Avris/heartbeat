import dotenv from 'dotenv';
import buildConfigDefaults from '../../config/config.defaults.mjs';
import connectDb from '../db.mjs';
import path from 'path';
import {Cache, ensureDirectoryExists, pathToAbsolute} from "../helpers.mjs";
import initialise from './initialise.mjs';


export default (options, action) => async () => {
    const timeout = setTimeout(() => {
        console.log('Process exceeded 10 minutes. Exiting...');
        process.exit(1);
    }, 10 * 60 * 1000);

    const dir = pathToAbsolute(process.cwd(), options.dir || '.');
    const paths = {
        config: pathToAbsolute(dir, options.config),
        data: await ensureDirectoryExists(pathToAbsolute(dir, options.data)),
        public: await ensureDirectoryExists(pathToAbsolute(dir, options.public)),
    }
    dotenv.config({path: path.resolve(dir, '.env')});

    if (options.init) {
        await initialise(paths.config);
    }

    Cache.dir = paths.data;

    const config = {
        ...await buildConfigDefaults(),
        ...await (await import(paths.config)).default(paths),
        paths,
    };

    let db;

    try {
        db = await connectDb(path.resolve(config.paths.data, 'database.sqlite'));
        await action(config, db, options);
    } catch (err) {
        if (typeof(err) === 'string') { err = new Error(err); }
        for (let notifier of config.notifiers) {
            await notifier.notifyError(err, config);
        }
    } finally {
        if (db) {
            await db.close();
        }
    }

    const formatMemoryUsage = (data) => `${Math.round(data / 1024 / 1024 * 100) / 100} MB`;
    console.log('Memory usage: ' + formatMemoryUsage(process.memoryUsage().rss));

    clearTimeout(timeout);

    process.nextTick(() => process.exit(0));  // parallelisation is a weird beast… i give up 😅
}
