import path from 'path';
import url from 'url';

export const LIB_DIR = path.dirname(path.dirname(url.fileURLToPath(import.meta.url)));

export const STATUS = {
    online: 1,
    offline: -1,
    unknown: 0,
}

export const GetStatusName = (status) => Object.keys(STATUS).find(key => STATUS[key] === status);

export const DEFAULT_TIMEOUT = 3000;
