import sqlite3 from 'sqlite3';
import { open } from 'sqlite';
import SQL from 'sql-template-strings';

export default async (path) => {
    const db = await open({
        filename: path,
        driver: sqlite3.Database
    });

    db.configure('busyTimeout', 3000);
    await db.exec('PRAGMA journal_mode = WAL;');

    await db.get(SQL`
        CREATE TABLE IF NOT EXISTS checks (
            page TEXT NOT NULL,
            timestamp INTEGER NOT NULL,
            status INTEGER NOT NULL,
            responseTime INTEGER NOT NULL,
            PRIMARY KEY(page, timestamp)
        );
    `);

    await db.get(SQL`
        CREATE TABLE IF NOT EXISTS recents (
            timestamp INTEGER PRIMARY KEY NOT NULL,
            status INTEGER NOT NULL
        );
    `);

    await db.get(SQL`
        CREATE TABLE IF NOT EXISTS dailies (
            page TEXT NOT NULL,
            date TEXT NOT NULL,
            checksCount INTEGER NOT NULL,
            uptimePercent DECIMAL(4, 2) NOT NULL,
            responseTimeAvg INTEGER NOT NULL,
            PRIMARY KEY (page, date)
        );
    `);

    return db;
}
