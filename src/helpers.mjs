import { promises as fs } from 'fs';
import path from 'path';

export function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

export const buildProxy = (obj) => new Proxy(obj, {
    get: (target, name, receiver) => {
        return name in target ? target[name] : target.base[name];
    },
    set: (target, name, value, receiver) => {
        if (name in target) {
            target[name] = value;
        } else {
            target.base[name] = value;
        }
    },
});

export const pathToAbsolute = (base, rel) => path.isAbsolute(rel) ? rel : path.resolve(base, rel);

export const ensureDirectoryExists = async (dir) => {
    await fs.mkdir(dir, {recursive: true});
    return dir;
}

export const withTimeout = async (promise, timeout) => {
    let timer;
    try {
        const timeoutPromise = new Promise((_, reject) => {
            timer = setTimeout(() => {
                reject(new Error('Operation timed out'));
            }, timeout);
        });

        return await Promise.race([promise, timeoutPromise]);
    } finally {
        clearTimeout(timer);
    }
}

export class Cache {
    static dir = undefined;

    static async get(key, maxAgeSeconds, generator) {
        const path = `${Cache.dir}/${key}.json`;

        try {
            const fileStats = await fs.stat(path);
            const fileAgeSeconds = (new Date() - fileStats.mtime) / 1000;
            if (fileAgeSeconds > maxAgeSeconds) {
                throw 'Cache expired';
            }
        } catch (error) {
            const value = await generator();
            await fs.writeFile(path, JSON.stringify(value));
            return value;
        }

        return JSON.parse(await fs.readFile(path, 'utf-8'));
    }
}

export const getCloudflareIps = async () => {
    return {
        ipv4: await Cache.get(
            'cloudflare-ip4',
            24 * 60 * 60,
            async () => (await (await fetch('https://www.cloudflare.com/ips-v4')).text()).split('\n')
        ),
        ipv6: await Cache.get(
            'cloudflare-ip6',
            24 * 60 * 60,
            async () => (await (await fetch('https://www.cloudflare.com/ips-v6')).text()).split('\n')
        )
    }
}
