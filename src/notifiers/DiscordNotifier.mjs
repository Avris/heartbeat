import NotifierInterface from "./NotifierInterface.mjs";
import fetch from 'node-fetch';

export default class DiscordNotifier extends NotifierInterface {
    constructor(webhookUrl) {
        super();
        this.webhookUrl = webhookUrl;
    }

    async notify(overallStatus, pagesMessages, {pages}) {
        console.log('Sending Discord notification');
        await this._sendMessage(
            `**${overallStatus}**\n\n` +
            Object.entries(pagesMessages).map(
                ([page, message]) => ` – ${page}: ${message}`
            ).join('\n')
        );
    }

    async notifyError(err, config) {
        console.log('Sending Discord notification');
        await this._sendMessage('**Error**\n\n```' + err.stack + '```');
    }

    async _sendMessage(message) {
        const response = await fetch(this.webhookUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                content: message
            })
        });

        if (!response.ok) {
            throw `Failed to send notification: ${response.statusText}`;
        }
    }
}
