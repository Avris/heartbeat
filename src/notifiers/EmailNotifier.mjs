import nodemailer from 'nodemailer';
import NotifierInterface from "./NotifierInterface.mjs";

export default class EmailNotifier extends NotifierInterface {
    constructor(transport, from, to) {
        super();
        this.transporter = nodemailer.createTransport(transport, {from: from});
        this.to = to;
    }

    async notify(overallStatus, pagesMessages, {pages}) {
        console.log('Sending email to', this.to);
        await this.transporter.sendMail({
            to: this.to,
            subject: overallStatus,
            text: Object.entries(pagesMessages).map(([page, message]) => ` – ${page}: ${message}`).join('\n'),
            html: `<ul>` + Object.entries(pagesMessages).map(([page, message]) =>
                `<li><strong><a href="${pages[page].url}" target="_blank" rel="noopener">${page}</a></strong>: ${message}</li>
            `).join('') + `</ul>`,
        });
    }

    async notifyError(err, config) {
        console.log('Sending email to', this.to);
        await this.transporter.sendMail({
            to: this.to,
            subject: '[heartbeat] ' + err.message,
            text: err.stack,
            html: `<pre>${err.stack}</pre>`,
        });
    }
}
