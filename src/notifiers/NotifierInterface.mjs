export default class NotifierInterface {
    async notify(overallStatus, pagesMessages, config) {
        throw 'Not implemented';
    }

    async notifyError(err, config) {
        throw 'Not implemented';
    }
}
