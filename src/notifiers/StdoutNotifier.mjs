import chalk from "chalk";
import NotifierInterface from "./NotifierInterface.mjs";

export default class StdoutNotifier extends NotifierInterface {
    async notify(overallStatus, pagesMessages, config) {
        console.log(chalk.blue(overallStatus));
        console.log(pagesMessages);
    }

    async notifyError(err, config) {
        console.error(chalk.red(err.message));
        console.error(chalk.red(err.stack));
    }
}
