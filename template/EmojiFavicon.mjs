export default (emoji) => {
    const svg = `
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100">
            <text y=".9em" font-size="90">${emoji}</text>
        </svg>
    `;

    return `<link rel="shortcut icon" href="data:image/svg+xml,${encodeURIComponent(svg)}" type="image/svg+xml">`;
}
