import Page from './Page.mjs';
import Style from './Style.mjs';
import Link from "./Link.mjs";
import T from "./Translations.mjs";
import EmojiFavicon from "./EmojiFavicon.mjs";
import Script from "./Script.mjs";

export default async ({status, pages, period, config}) => {
    const { periods, project, slotsCount } = config;

    return `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8"/>
            <title>${T.title}</title>
            <meta name="description" content="${T.description}" />
            <meta name="keywords" content="${T.keywords}" />
            <meta name="author" content="Andrea Vos <andrea@avris.it>" />
            <meta name="viewport" content="width=device-width, initial-scale=1">
            ${EmojiFavicon(T.status[status].emoji)}
            ${await Style()}
        </head>
        <body class="folded" style="--slots: ${slotsCount}">
            <header>
                <h1 class="${status}">
                    <a href="${Link('index.html', config)}">
                        ${T.status[status].emoji} ${T.title}
                    </a>
                    <small>${project}</small>
                </h1>
                
                <ul>
                    ${Object.keys(periods).map(p => 
                        `<li><a href="${Link(p + '.html', config)}" ${p === period ? 'class="active"' : ''}>${p}</a></li>`
                    ).join(' ')}
                    <li class="fold"><a href="#">⊖</a></li>
                    <li class="unfold"><a href="#">⊕</a></li>
                </ul>
            </header>
            <main>
                <ul>
                    <li class="overall ${status}">
                        ${T.status[status].overall}
                    </li>
                    ${Object.entries(pages).map(([name, page]) => 
                        Page({name, ...page, slotDuration: periods[period] / slotsCount})
                    ).join('')}
                </ul>
            </main>
            <footer>
                <ul>
                    <li><strong>${T.title}</strong> – ${T.description}</li>
                </ul>
                <ul>
                    <li>${T.footer.author} <a href="https://avris.it" target="_blank" rel="noopener">Andrea Vos</a></li>
                    <li>${T.footer.license}: <a href="https://oql.avris.it/license.tldr?c=Andrea%20Vos%7Chttps://avris.it" target="_blank" rel="noopener">OQL</a></li>
                    <li><a href="https://gitlab.com/Avris/heartbeat" target="_blank" rel="noopener">${T.footer.source}</a></li>
                    <li><a href="${Link(period + '.json', config)}">${T.footer.api}</a></li>
                    <li>
                        ${T.footer.support}:
                        <a href="https://bunq.me/AndreaVos" target="_blank" rel="noopener">bank</a>,
                        <a href="https://paypal.me/AndreAvris" target="_blank" rel="noopener">PayPal</a>
                    </li>
                </ul>
            </footer>
            ${Script()}
        </body>
        </html>
    `;
}
