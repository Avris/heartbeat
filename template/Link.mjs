export default (to, { baseUrl, hideExtensions }) => {
    let url = baseUrl + to;
    if (hideExtensions) {
        url = url.replace(/index\.html$/, '').replace(/\.html$/, '');
    }

    return url;
}
