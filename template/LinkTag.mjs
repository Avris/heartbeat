export default (text, url) => {
    if (!url) {
        return text;
    }
    return `<a href="${url}" target="_blank" rel="noopener">${text}</a>`;
}
