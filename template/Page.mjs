import Slot from './Slot.mjs';
import T from './Translations.mjs';
import {GetStatusName, STATUS} from "../src/const.mjs";
import LinkTag from "./LinkTag.mjs";

export default (page) => {
    let minResponseTime = Infinity;
    let maxResponseTime = 0;
    for (let slot of Object.values(page.slots)) {
        if (slot.avgResponseTime < minResponseTime) {
            minResponseTime = slot.avgResponseTime;
        }
        if (slot.avgResponseTime > maxResponseTime) {
            maxResponseTime = slot.avgResponseTime;
        }
    }
    if (minResponseTime > maxResponseTime) {
        minResponseTime = 0;
        maxResponseTime = 1000;
    }

    return `<li class="${page.status}">
        <header>
            <h2 class="${page.status}">${LinkTag(page.name, page.url)}</h2>
            <div>${T.uptime}: <strong>${page.uptime}%</strong></div>
            <div>${T.avgResponseTime}: <strong>${page.avgResponseTime} ms</strong></div>
        </header>
        <aside class="status-${page.status}">${T.status[page.status].label}</aside>
        <main class="slots-wrapper">
            ${Object.entries(page.slots).map(
                ([slotStart, slotStats]) => Slot({start: slotStart, ...slotStats, duration: page.slotDuration}, minResponseTime, maxResponseTime)
            ).join('')}
        </main>
    </li>`;
}
