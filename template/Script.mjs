export default () => `<script>
    const slots = document.querySelectorAll('.slot');
    const wrappers = document.querySelectorAll('.slots-wrapper');
    slots.forEach(slot => slot.addEventListener('click', function() {
        const wasSelected = this.classList.contains('selected');
        slots.forEach(s => s.classList.remove('selected'));
        if (!wasSelected) {
            this.classList.add('selected');
        }
       
        wrappers.forEach(s => s.classList.remove('selected'));
        if (!wasSelected) {
            this.parentNode.classList.add('selected')
        }
    }));

    function index(el) {
        let i = -1;
        while (el) {
            el = el = el.previousElementSibling;
            i++;
        }
        return i;
    }
    
    document.addEventListener('keydown', function (event) {
        const selectedSlot = document.querySelector('.slot.selected');
        const selectedWrapper = document.querySelector('.slots-wrapper.selected');
        if (!selectedSlot) { return; }
        
        let newlySelectedSlot;
        let newlySelectedWrapper;
        switch (event.key) {
            case 'ArrowLeft':
                newlySelectedSlot = selectedSlot.previousElementSibling;
                event.preventDefault();
                event.stopPropagation();
                break;
            case 'ArrowRight':
                newlySelectedSlot = selectedSlot.nextElementSibling;
                event.preventDefault();
                event.stopPropagation();
                break;
            case 'ArrowUp':
                newlySelectedWrapper = selectedWrapper.parentElement.previousElementSibling.querySelector('.slots-wrapper');
                if (!newlySelectedWrapper) { return; }
                newlySelectedSlot = newlySelectedWrapper.querySelectorAll('.slot')[index(selectedSlot)];
                event.preventDefault();
                event.stopPropagation();
                break;
            case 'ArrowDown':
                newlySelectedWrapper = selectedWrapper.parentElement.nextElementSibling.querySelector('.slots-wrapper');
                if (!newlySelectedWrapper) { return; }
                newlySelectedSlot = newlySelectedWrapper.querySelectorAll('.slot')[index(selectedSlot)];
                event.preventDefault();
                event.stopPropagation();
                break;
            case 'Escape':
                selectedSlot.classList.remove('selected');
                selectedWrapper.classList.remove('selected');
                event.preventDefault();
                event.stopPropagation();
        }

        if (newlySelectedSlot) {
            selectedSlot.classList.remove('selected');
            newlySelectedSlot.classList.add('selected')
        }
        if (newlySelectedWrapper) {
            selectedWrapper.classList.remove('selected');
            newlySelectedWrapper.classList.add('selected')
        }
    });
    
    document.querySelector('.fold > a').addEventListener('click', e => {
        document.body.classList.add('folded');
        e.preventDefault();
        e.stopPropagation();
    });

    document.querySelector('.unfold > a').addEventListener('click', e => {
        document.body.classList.remove('folded');
        e.preventDefault();
        e.stopPropagation();
    });
    
    setInterval(() => window.location.reload(), 60_000);
</script>`;
