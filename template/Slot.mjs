import T from "./Translations.mjs";
import {GetStatusName, STATUS} from "../src/const.mjs";

export default (slot, minResponseTime, maxResponseTime) => {
    const minHeight = 20;
    const maxHeight = 100;

    let height;
    if (minResponseTime === maxResponseTime) {
        height = maxHeight
    } else {
        height = minHeight + (maxHeight - minHeight) * (slot.avgResponseTime - minResponseTime) / (maxResponseTime - minResponseTime);
    }

    slot.start = parseInt(slot.start);
    const slotStart = new Date(slot.start * 1000);
    const slotEnd = new Date((slot.start + slot.duration * 60) * 1000);

    const onlinePercentage = 10 * Math.round(10 * slot.onlineChecks / slot.checks);

    return `<span class="slot">
        <span class="status status-${slot.status} ${slot.status === GetStatusName(STATUS.offline) ? 'offline-percent-' + onlinePercentage : ''}" style="height: ${Math.round(height)}%;">
            <span class="tooltip">
                ${slotStart.toISOString().replace(/T.*$/, '')}<br/>
                ${slotStart.toISOString().replace(/^.*T/, '').replace(/:\d\d\.000Z$/, '')} –
                ${slotEnd.toISOString().replace(/^.*T/, '').replace(/:\d\d\.000Z$/, '')}
                <br/>
                <strong>${T.status[slot.status].label} (<small>✔</small> ${slot.onlineChecks}/${slot.checks})</strong>
                <br/>
                ${T.avg} ${slot.avgResponseTime} ms
            </span>
        </span>
    </span>`;
}
