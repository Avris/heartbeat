import { Cache } from "../src/helpers.mjs";
import { LIB_DIR } from "../src/const.mjs";

export default async () => `<style>${await Cache.get('style.css', 24*60*60, async () => {
    const sass = await import('sass');
    const result = sass.compile(LIB_DIR + '/template/style.scss');
    return result.css;
})}</style>`;
