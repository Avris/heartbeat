export default {
    title: 'heartbeat',
    description: 'lightweight website monitoring',
    keywords: 'uptime, status, website, page, monitoring, response time, online, offline',
    status: {
        online: {
            emoji: '💚',
            label: 'online',
            overall: 'all systems operational'
        },
        offline: {
            emoji: '🛑',
            label: 'offline',
            overall: 'system down'
        },
        unknown: {
            emoji: '😕',
            label: 'unknown',
            overall: 'system partially degraded'
        },
    },
    uptime: 'uptime',
    avgResponseTime: 'avg response time',
    avg: 'avg',
    footer: {
        author: 'app by',
        license: 'license',
        source: 'source code',
        api: 'JSON',
        support: 'support the project'
    },
};
